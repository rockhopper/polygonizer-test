# Grids

- Generate a 2d array that can be polygonized.
- Step through that array to find border
    - This will generate Line Segments and place them in a dictionary.
    - `lines[{starPoint}] = {endpoint}`
- Step through borders to find contiguous lines.
    - Take first index, find it's endpoint, walk through until you've returned to starting point
    - Loop until every index is cleaned out
    - k


# Island detection

## Fill Algorithm

- Find Starting Point
- create dataList, mark starting point
- create edgeList, init with starting point
- Loop while Edge List isn't empty
    - scan all neighbors
        - if neighbor is full and not in dataList
            - add to data list
            - add to newEdgeList
    - edgeList = newEdge






## Fill Algorithm

- init loop state
    - currentIsland
    - edge
- loop
    - for each cell on edge
        - newEgde = []
        - check neighbors, 
            - if they're in old grid and not in current grid, 
                - remove them from the old
                - add them to currentIsland
                - remove them old island
        - edge = newEdge